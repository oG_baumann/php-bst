<?php

class bst {
	
	public   $root;
	private  $arr = array();

	public function __construct($root = null) {
		$this->root = $root;
	}

	private function node ($value) {
		return Array(
			value 	=> $value,
			idx  	=> 1,
			left	=> null,
			right 	=> null
		);
	}

	public function add ($value) {
		if (!$this->root) {
			$this->root = $this->node($value);
		} else {
			$node = &$this->root;
			while($node) {
				if($value > $node['value']) {
					if(!$node['right']) {
						$node['right'] = $this->node($value);
						$this->depth++;
						return;
					} else {
						$node = &$node['right'];
					}
				} elseif ($value < $node['value']) {
					if(!$node['left']) {
						$node['left'] = $this->node($value);
						return;
					} else {
						$node = &$node['left'];
					}
				} elseif ($value == $node['value']) {
					$node['idx']++;
					return;
				}
			}
		}
	} 

	public function preOrder ($node) {
		$node = $node ?: $this->root;
		if($node == $this->root) $this->arr = array();
		if($node['value']) {
			array_push($this->arr, $node['value']);
		}
		if($node['left']) {
			$this->preOrder($node['left']);
		}
		if($node['right']) {
			$this->preOrder($node['right']);
		}
	}

	public function inOrder ($node) {
		$node = $node ?: $this->root;
		if($node == $this->root) $this->arr = array();
		if($node['left']) {
			$this->inOrder($node['left']);
		}
		if($node['value']) {
			array_push($this->arr, $node['value']);
		}		
		if($node['right']) {
			$this->inOrder($node['right']);
		}
	}	

	public function postOrder ($node) {
		$node = $node ?: $this->root;
		if($node == $this->root) $this->arr = array();
		if($node['left']) {
			$this->postOrder($node['left']);
		}	
		if($node['right']) {
			$this->postOrder($node['right']);
		}
		if($node['value']) {
			array_push($this->arr, $node['value']);
		}			
	}

	public function min ($node) {
		$node = $node ?: $this->root;
		if($node['left']){
			return $this->min($node['left']);
		} else {
			return $node['value'];
		}
	}

	public function max ($node) {
		$node = $node ?: $this->root;
		if($node['right']){
			return $this->max($node['right']);
		} else {
			return $node['value'];
		}
	}

	public function getIt () {
		return $this->arr;
	}

}

$tree = new bst();
$i = 0;

while($i < 100) {
	$tree->add(rand(1,1000));
	$i++;
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">	
	<title>PHP BST</title>
</head>
<body>
	<style type="text/css">
		body > div {
			display: block;
			margin: 0 auto;
			width: 100%;
			max-width: 1030px;
			padding: 15px;
			box-sizing: border-box;
			word-break: break-all;
		}
	</style>
	<div>
		<h1>Full BST</h1>
		<div><?php echo json_encode($tree->root) ?></div>
		<h2>Pre-order Traversal</h2>
		<div>
			<?php
				$tree->preOrder();
				echo json_encode($tree->getIt());
			?>
		</div>
		<h2>In-order Traversal</h2>
		<div>
			<?php
				$tree->inOrder();
				echo json_encode($tree->getIt());
			?>		
		</div>
		<h2>Post-order Traversal</h2>
		<div>
			<?php
				$tree->postOrder();
				echo json_encode($tree->getIt());
			?>		
		</div>
		<h3>Root</h3>
		<div>
			<?php
				echo $tree->root['value'];
			?>			
		</div>
		<h3>Max</h3>
		<div>
			<?php
				echo $tree->max();
			?>			
		</div>
		<h3>Min</h3>
		<div>
			<?php
				echo $tree->min();
			?>			
		</div>
	</div>		
</body>
</html>

